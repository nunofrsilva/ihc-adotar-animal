import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.scss';
//import App from './App';
import Main from './components/Main.js'
import {BrowserRouter} from "react-router-dom";

ReactDOM.render(
    <BrowserRouter><Main /></BrowserRouter>,
    document.getElementById('root')
);

