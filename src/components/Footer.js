import React from "react";
import "../styles/Footer.scss"
import {Grid} from "@material-ui/core";
import {Link} from "react-router-dom";
import {faBars, faEnvelope} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import FacebookIcon from '@material-ui/icons/Facebook';
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import InstagramIcon from '@material-ui/icons/Instagram';

const footerMenu = [
    {
        label: "> Adotar",
        href: "/lista-animais",
    },
    {
        label: "> Como ajudar?",
        href: "/not-implemented",
    },
    {
        label: "> Política de Privacidade",
        href: "/politica-de-privacidade",
    },
    {
        label: "> Termos de utilização",
        href: "/not-implemented",
    }
];

const Footer = () => {
    return (
        <footer>
            <Grid container>
               <Grid item xs={12} sm={4} md={4}>
                   <div className={"footer_logo"}>
                       <h2>Adotar Animal</h2>
                       <p>
                           Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                       </p>
                   </div>
               </Grid>
                <Grid item container xs={12} sm={4} md={4} justify={"center"} className={"footer_links_uteis_container"}>
                    <div className={"footer_links_uteis"}>
                        <h3>Links Úteis</h3>
                        <ul>
                            {footerMenu.map((d,index )=> {
                                return (
                                    <li key={index}>
                                        <Link to={d.href}>{d.label}</Link>
                                    </li>
                                );
                            })}
                        </ul>
                    </div>
                </Grid>
                <Grid item container xs={12} sm={4} md={4} justify={"center"} className={"footer_contacts_container"}>
                    <div className={"footer_contacts"}>
                        <h3>Contacte-nos</h3>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                        </p>
                        <p>
                            <FontAwesomeIcon icon={faEnvelope} size={'lg'} /> info@adotaranimal.pt
                        </p>
                        <div className={"footer_links"}>
                            <FacebookIcon fontSize={"large"}/>
                            <WhatsAppIcon fontSize={"large"}/>
                            <InstagramIcon fontSize={"large"}/>
                        </div>
                    </div>


                </Grid>
            </Grid>

        </footer>
    );
};

export default Footer;