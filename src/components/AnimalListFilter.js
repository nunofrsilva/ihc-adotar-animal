import React from 'react';
import {Container, Grid} from "@material-ui/core";
import AnimalCard from "./AnimalCard";

const AnimalListFilter = (props) => {
    const data = props.data.sort((a, b) => {
        if (a.id > b.id)
            return 1;
        if (a.id < b.id)
            return -1;
        return 0;
    });

    let isEmpty = data.length > 0;

    const renderList = () => {
        return (
            data.slice(0, 12).map((data, index) => {
                return (
                    <Grid item key={data.id} lg={4}>
                        <AnimalCard data={data} />
                    </Grid>
                );
            })
        );

    };

    return (
        <Container className={"adopt-home-block"}>
            <Grid container justify={"center"}>
                <h2>Adota-me!</h2>
                <Grid container justify={"center"} spacing={3}>
                    {isEmpty
                        ? renderList()
                        : <h3 className={"no-results"}>Sem resultados! <br /> Por favor redefina a sua pesquisa.</h3>
                    }
                </Grid>
            </Grid>
        </Container>
    );
};

export default AnimalListFilter;