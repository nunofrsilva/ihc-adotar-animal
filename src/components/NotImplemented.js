import React from 'react';
import Header from "./Header";
import Footer from "./Footer";
import {Container} from "@material-ui/core";
import PetsIcon from '@material-ui/icons/Pets';
import "../styles/Page.scss"

const NotImplemented = () => {
    return (
        <div>
            <Header />
            <Container>
                <div className={"wrapper"}>
                    <PetsIcon />
                    <h1>Página não implementada neste protótipo.</h1>
                </div>

            </Container>
            <Footer />
        </div>
    );

}

export default NotImplemented;