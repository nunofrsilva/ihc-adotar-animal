import React, {useEffect} from 'react';
import animalList from "../data/animalList";
import Header from "./Header";
import GalleryDetail from "./GalleryDetail";
import {
    Breadcrumbs,
    Button, Checkbox,
    Container,
    FormControl, FormControlLabel,
    FormHelperText, FormLabel,
    Grid,
    Input,
    InputLabel,
    MenuItem, Radio, RadioGroup,
    Select, Typography
} from "@material-ui/core";
import {Link, useParams} from "react-router-dom";
import Footer from "./Footer";
import "../styles/AdotarAnimal.scss"
import distrcit from "../data/distritos";
import SubmitModal from "./SubmitModal";
import SimpleModal from "./SimpleModal";

const AdoptAnimal = (props) => {
    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);
    let { id } = useParams();
    const animalDetails = animalList.find(d => d.id === parseInt(id));

    const districtList = distrcit.filter(d => d.level === 1);
    const county = distrcit.filter(d => d.level === 2);

    function handleSubmit(event) {
        event.preventDefault();
        console.log( 'Email:');
        // You should see email and password in console.
        // ..code to submit form to backend here...

    }


    return (
        <div>
            <Header />
            <Container>
                <Breadcrumbs aria-label="breadcrumb" className={"breadcrumbs"}>
                    <Link className={"previous"} to={"/"}>
                        Página principal
                    </Link>
                    <Link className={"previous"} to={"/lista-animais"}>
                        Adotar
                    </Link>
                    <Link className={"previous"} to={"/animal/"+animalDetails.id}>
                        Detalhes
                    </Link>
                    <Typography className={"selected"} color="textPrimary">Adotar {animalDetails.name}</Typography>
                </Breadcrumbs>
            </Container>
            <Container className={"adotpt-container"}>
                <Grid container className={"adopt-header"} spacing={4}>
                    <Grid item xs={12} sm={12} md={6} lg={4}>
                        <div className={"thumbnail"}>
                            <img src={animalDetails.photos[0]} />
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={8}>
                        <div>
                            <h1>{animalDetails.name}</h1>
                        </div>
                        <div className={"paper-container"}>
                            <h3>Características</h3>
                            <div className={"details-container"}>
                                <div>
                                    <strong>Animal</strong> <br />
                                    {(animalDetails.type === "cat") ? "Gato" : "Cão"}
                                </div>
                                <div>
                                    <strong>Idade</strong> <br />
                                    {animalDetails.age}
                                </div>
                                <div>
                                    <strong>Sexo</strong> <br />
                                    {animalDetails.gender}
                                </div>
                                <div>
                                    <strong>Tamanho</strong> <br />
                                    {animalDetails.size}
                                </div>
                                <div>
                                    <strong>Esterilizado</strong> <br />
                                    {animalDetails.sterilized}
                                </div>
                                <div>
                                    <strong>Chip</strong> <br />
                                    {animalDetails.chip}
                                </div>
                            </div>
                        </div>
                    </Grid>
                </Grid>
                <Grid container className={"adopt-content"} spacing={4}>
                    <Grid item xs={12} sm={12} md={12} lg={4}>
                        <div className={"paper-container"}>
                            <h3>Antes de adotar pense:</h3>
                            <div className={"details-container"}>
                                <ul className={"list-after-adopt"}>
                                    <li>
                                        Tem disponibilidade para providenciar ao seu animal a educação, a companhia, a atenção e
                                        o carinho de que ele precisa?
                                    </li>
                                    <li>
                                        Está preparado para lidar com eventuais "asneiras" em casa (principalmente se adotar um animal
                                        muito jovem)?
                                    </li>
                                    <li>
                                        É capaz de fornecer o acompanhamento veterinário regular ao seu animal (vacinação, desparatização interna e externa...)?
                                    </li>
                                    <li>
                                        Está disposto a assegurar o devido tratamento se o seu animal tiver que ser operado ou tiver algum problema grave de saúde?
                                    </li>
                                    <li>
                                        Tem espaço exterior suficiente ou está disponível para levar o seu cão a passear várias vezes ao dia?
                                    </li>
                                    <li>
                                        O que fará ao seu animal quando quiser ir de férias ou tiver de se ausentar?
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={8} className={"form-container"}>
                        <div className={"paper-container"}>
                            <h3>Formulário de adoção</h3>
                            <div className={"details-container"}>
                                <form onSubmit={handleSubmit}>
                                    <Grid container spacing={3}>
                                        <Grid item xs={12} sm={12} md={12} lg={12}>
                                            <FormControl>
                                                <InputLabel htmlFor="name">Nome</InputLabel>
                                                <Input id="name"/>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={12} md={12} lg={6}>
                                            <FormControl>
                                                <InputLabel htmlFor="email">Email</InputLabel>
                                                <Input id="email" />
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={12} md={12} lg={6}>
                                            <FormControl>
                                                <InputLabel htmlFor="phone">Phone</InputLabel>
                                                <Input id="phone" />
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={12} md={12} lg={6}>
                                            <FormControl>
                                                <InputLabel id="select-district-label">Distrito</InputLabel>
                                                <Select
                                                    labelId="select-district-label"
                                                    id="select-county"
                                                    defaultValue=""
                                                >
                                                    {districtList.map((d, index)=> (
                                                        <MenuItem key={index} value={Number(d.code)}>{d.name}</MenuItem>
                                                    ))}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={12} md={12} lg={6}>
                                            <FormControl>
                                                <InputLabel id="select-county-label">Concelho</InputLabel>
                                                <Select
                                                    labelId="select-county-label"
                                                    id="select-county"
                                                    defaultValue=""
                                                >
                                                    {county.map((d, index)=> (
                                                        <MenuItem key={index} value={Number(d.code)}>{d.name}</MenuItem>
                                                    ))}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={12} md={12} lg={12}>
                                            <FormControl component="fieldset">
                                                <FormLabel component="legend">Pergunta 1</FormLabel>
                                                <RadioGroup aria-label="gender" name="gender1" className={"radio-inline"}>
                                                    <FormControlLabel value="female" control={<Radio />} label="Opção 1" />
                                                    <FormControlLabel value="male" control={<Radio />} label="Opção 2" />
                                                    <FormControlLabel value="other" control={<Radio />} label="Opção 3" />
                                                </RadioGroup>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={12} md={12} lg={12}>
                                            <FormControlLabel control={<Checkbox name="checkedC" />} label="Li e aceito a politica de privacidade, termos e condições" />

                                        </Grid>
                                        <Grid item xs={12} sm={12} md={12} lg={12} className={"submit-button-container"}>
                                            <Button
                                                type={"submit"}
                                                variant="contained"
                                                color="primary"
                                                className={"submit-button"}
                                            >
                                                Submeter
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </form>
                            </div>
                        </div>
                    </Grid>
                </Grid>
            </Container>
            <Footer />
        </div>
    );
}

export default AdoptAnimal;