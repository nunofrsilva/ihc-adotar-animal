import React, {Component, useEffect, useState} from "react";
import {
    Button,
    Container,
    Drawer,
    Grid,
    IconButton,
    InputBase,
    Link,
    MenuItem
} from '@material-ui/core';
import '../styles/Header.scss'
import SearchIcon from '@material-ui/icons/Search';
import {Link as RouterLink, NavLink} from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons'

const headersData = [
    {
        label: "Página Inicial",
        href: "/",
    },
    {
        label: "Adotar",
        href: "/lista-animais",
    },
    {
        label: "Perguntas Frequentes",
        href: "/not-implemented",
    },
    {
        label: "Como ajudar?",
        href: "/not-implemented",
    },
    {
        label: "Contactos",
        href: "/not-implemented",
    }
];

const Header = () => {

    const [state, setState] = useState({
        mobileView: false,
        drawerOpen: false,
    });

    const { mobileView, drawerOpen } = state;

    useEffect(() => {
        const setResponsiveness = () => {
            return window.innerWidth < 900
                ? setState((prevState) => ({ ...prevState, mobileView: true }))
                : setState((prevState) => ({ ...prevState, mobileView: false }));
        };

        setResponsiveness();

        window.addEventListener("resize", () => setResponsiveness());
    }, []);

    // Desktop version
    const displayDesktop = () => {
        return (
                <div>
                    <Grid container justify="center" className={'header_container'}>
                        <Grid item>
                            <Link href={'/'}>
                                <h1>Adotar Animal</h1>
                            </Link>
                        </Grid>
                    </Grid>
                    <Grid container justify="center" className={'header_menu'}>
                        <Container>
                            <Grid container justify="center" alignItems="center" >
                                <Grid item sm={12} md={10} lg={10} className={'top_menu'}>
                                    {getMenuLinks()}
                                </Grid>
                                <Grid item md={2} lg={2} className={'search_container'}>
                                    <div className={'search_icon'}>
                                        <SearchIcon />
                                    </div>
                                    <InputBase
                                        placeholder="Search…"
                                        classes={{
                                            root: 'inputRoot',
                                            input: 'inputInput',
                                        }}
                                        inputProps={{ 'aria-label': 'search' }}
                                    />
                                </Grid>
                            </Grid>
                        </Container>
                    </Grid>
                </div>
        );
    };

    const displayMobile = () => {
        const handleDrawerOpen = () =>
            setState((prevState) => ({ ...prevState, drawerOpen: true }));
        const handleDrawerClose = () =>
            setState((prevState) => ({ ...prevState, drawerOpen: false }));

        return (
            <div>
                <Grid container justify="center" className={'header_container'}>
                    <Grid container>
                        <Grid container xs={1} sm={1} md={1} justify={"center"}>
                            <IconButton
                                {...{
                                    edge: "start",
                                    //color: "inherit",
                                    "aria-label": "menu",
                                    "aria-haspopup": "true",
                                    onClick: handleDrawerOpen,
                                }}
                            >
                                <FontAwesomeIcon icon={faBars} size={'lg'} className={"menu_btn_bar"}/>
                            </IconButton>
                        </Grid>

                        <Grid item xs={11} sm={11} md={10} alignItems={"center"} className={"header_logo"}>
                            <Link href={'/'}>
                                <h1>Adotar Animal</h1>
                            </Link>
                        </Grid>

                        <Drawer
                            {...{
                                anchor: "left",
                                open: drawerOpen,
                                onClose: handleDrawerClose,
                                className: "mobile-drawer"
                            }}
                        >
                            <div className={'drawerContainer'}>{getDrawerChoices()}</div>
                        </Drawer>
                    </Grid>
                </Grid>
            </div>
        );
    };


    const getMenuLinks = () => {
        return headersData.map(({ label, href }) => {
            return (
                <NavLink
                    {...{
                        key: label,
                        to: href,
                        className: 'menuButton',
                        activeClassName: "active",
                        exact: true
                    }}
                >{label}</NavLink>
            );
        });
    };

    const getDrawerChoices = () => {
        return headersData.map(({ label, href }) => {
            return (
                <NavLink
                    {...{
                        component: RouterLink,
                        to: href,
                        color: "menuButton",
                        style: { textDecoration: "none" },
                        activeClassName: "active",
                        key: label,
                    }}
                >
                    <MenuItem>{label}</MenuItem>
                </NavLink>
            );
        });
    };

    return (
        <header className={'header'}>
            {mobileView ? displayMobile() : displayDesktop()}
        </header>
    );
}

export default Header;