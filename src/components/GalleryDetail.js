import React from 'react';
import {Container, Grid} from "@material-ui/core";
import "../styles/GalleryDetail.scss"

const GalleryDetail = (props) => {
    const data = props.data;
    return (
        <div className={"gallery-detail-container"}>
            <Grid container spacing={1}>
                <Grid item xs={5} sm={5} md={5} lg={5}>
                    <img src={data[0]} className={"first-image"} />
                </Grid>
                <Grid item xs={7} sm={7} md={7} lg={7}>
                    <Grid container spacing={1}>
                        <Grid item xs={6} sm={6} md={6} lg={6} className={"image-container"}>
                            <img src={data[1]} />
                        </Grid>
                        <Grid item xs={6} sm={6} md={6} lg={6} className={"image-container"}>
                            <img src={data[2]} />
                        </Grid>
                        <Grid item xs={6} sm={6} md={6} lg={6} className={"image-container"}>
                            <img src={data[3]} />
                        </Grid>
                        <Grid item xs={6} sm={6} md={6} lg={6} className={"image-container"}>
                            <img src={data[4]} />
                        </Grid>
                    </Grid>

                </Grid>
            </Grid>


        </div>
    );

}

export default GalleryDetail;