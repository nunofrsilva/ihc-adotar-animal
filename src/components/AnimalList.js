import React, {Component} from 'react';
import Header from "./Header";
import Footer from "./Footer";
import AnimalListFilter from "./AnimalListFilter";
import animals from "../data/animalList";
import {Breadcrumbs, Container, FormControl, Grid, InputLabel, MenuItem, Select, Typography} from "@material-ui/core";
import '../styles/AnimalList.scss'
import FilterListIcon from '@material-ui/icons/FilterList';
import {Link} from "react-router-dom";

class AnimalList extends Component {
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    constructor(props) {
        super(props);
        this.state = {
            type: "",
            age: "",
            size: "",
            gender: "",
            localization: "",
            filterList: animals

        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        const attribute = event.target.name;
        let value = event.target.value;
        let filterList = [];
        if (value !== "" && value !== null && value !== "all") {
            filterList = this.resetListWithNewFilterValues(attribute, value);
        } else {
            filterList = this.removeFilter(attribute);
        }

        this.setState({[attribute]: value, filterList});
    }

    resetListWithNewFilterValues(attribute, value) {
        let filters = [
            {"filter": "type", "value": this.state.type},
            {"filter": "age", "value": this.state.age},
            {"filter": "size", "value": this.state.size},
            {"filter": "gender", "value": this.state.gender},
            {"filter": "localization", "value": this.state.localization},
        ];
        filters = filters.map(d => {
           if (d.filter === attribute) {
               d.value = value;
               return d;
           }
           return d;
        });
        return this.newFilterList(filters);
    }

    removeFilter(removeFilter) {
        let filters = [
                {"filter": "type", "value": this.state.type},
                {"filter": "age", "value": this.state.age},
                {"filter": "size", "value": this.state.size},
                {"filter": "gender", "value": this.state.gender},
                {"filter": "localization", "value": this.state.localization},
            ];

        // Remove filter
        filters = filters.filter(d => d.filter !== removeFilter);
        console.log("filters", filters)
        return this.newFilterList(filters);
    }

    newFilterList(filters) {
        let list = animals;
        filters.forEach(data => {
            if (data.value !== "" && data.value !== null && data.value !== "all") {
                list = list.filter(d => d[data.filter] === data.value);
                console.log("list", list)
            }
        });
        return list;
    }

    render() {
        const age = [...new Set(animals.map(item => item.age))];
        const size = [...new Set(animals.map(item => item.size))];
        const gender = [...new Set(animals.map(item => item.gender))];
        const localization = [...new Set(animals.map(item => item.localization))];
        return (
            <div>
                <Header/>
                <Container>
                    <Breadcrumbs aria-label="breadcrumb" className={"breadcrumbs"}>
                        <Link className={"previous"} to={"/"}>
                            Página principal
                        </Link>
                        <Typography className={"selected"} color="textPrimary">Adotar</Typography>
                    </Breadcrumbs>
                </Container>
                <Container className={"animal-filter-container"}>
                    <Grid container spacing={4} justify={"center"}>
                        <Grid item >
                            <FormControl className={"filter-selects"}>
                                <InputLabel id="select-type-label">Animal</InputLabel>
                                <Select
                                    labelId="select-type-label"
                                    id="select-type"
                                    name={"type"}
                                    value={this.state.type}
                                    onChange={this.handleChange}
                                >
                                    <MenuItem value={"all"}>Qualquer</MenuItem>
                                    <MenuItem value={"dog"}>Cão</MenuItem>
                                    <MenuItem value={"cat"}>Gato</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item>
                            <FormControl className={"filter-selects"}>
                                <InputLabel id="select-age-label">Idade</InputLabel>
                                <Select
                                    labelId="select-age-label"
                                    id="select-age"
                                    name={"age"}
                                    value={this.state.age}
                                    onChange={this.handleChange}
                                >
                                    <MenuItem value={"all"}>Qualquer</MenuItem>
                                    {age.map(item => (
                                        <MenuItem key={item} value={item}>{item}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item>
                            <FormControl className={"filter-selects"}>
                                <InputLabel id="select-size-label">Tamanho</InputLabel>
                                <Select
                                    labelId="select-size-label"
                                    id="select-size"
                                    name={"size"}
                                    value={this.state.size}
                                    onChange={this.handleChange}
                                >
                                    <MenuItem value={"all"}>Qualquer</MenuItem>
                                    {size.map(item => (
                                        <MenuItem key={item} value={item}>{item}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item>
                            <FormControl className={"filter-selects"}>
                                <InputLabel id="select-gender-label">Sexo</InputLabel>
                                <Select
                                    labelId="select-gender-label"
                                    id="select-local"
                                    name={"gender"}
                                    value={this.state.gender}
                                    onChange={this.handleChange}
                                >
                                    <MenuItem value={"all"}>Qualquer</MenuItem>
                                    {gender.map(item => (
                                        <MenuItem key={item} value={item}>{item}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item>
                            <FormControl className={"filter-selects"}>
                                <InputLabel id="select-local-label">Distrito</InputLabel>
                                <Select
                                    labelId="select-local-label"
                                    id="select-local"
                                    name={"localization"}
                                    value={this.state.localization}
                                    onChange={this.handleChange}
                                >
                                    <MenuItem value={"all"}>Qualquer</MenuItem>
                                    {localization.map(item => (
                                        <MenuItem key={item} value={item}>{item}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item className={"filter_search_container"}>
                            <Link>
                                <FilterListIcon  className={"search_icon"} fontSize={"large"}/>
                            </Link>
                        </Grid>
                    </Grid>

                </Container>
                <div className={"filter-list-container"}>
                    <AnimalListFilter data={this.state.filterList} />
                </div>
                <Footer />
            </div>
        );
    }
}

export default AnimalList;