import React from "react";
import GoogleMapReact from 'google-map-react';

const AnyReactComponent = ({ text }) => <div>{text}</div>;


const Map = (props) => {
    const { lat, lng } = props;
    return (
        // Important! Always set the container height explicitly
        <div style={{ height: '250px', width: '100%' }}>
            <GoogleMapReact
                bootstrapURLKeys={{ key: "AIzaSyDvCQf7iaoNDihX3Gl2H8v2-yHRccbzhU4"}}
                defaultCenter={{lat: lat, lng:lng}}
                defaultZoom={8}
            >
            </GoogleMapReact>
        </div>
    );
}

Map.defaultProps = {
    lat:38.74,
    lng: -9.195
}

export default Map;

