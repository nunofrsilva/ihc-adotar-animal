import React from 'react';
import {Box, Card, Container, Grid, Paper, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";
import '../styles/AdoptHomeBlock.scss'
import animalList from "../data/animalList";
import AnimalCard from "./AnimalCard";

const AdoptHomeBlock = () => {
    const data = animalList.sort((a, b) => a.id - b.id);
    return (
        <Container className={"adopt-home-block"}>
            <Grid container justify={"center"}>
                <h2>Adota-me!</h2>
                <Grid container justify={"center"} spacing={3}>
                    {data.slice(0, 6).map((data, index) => {
                        return (
                            <Grid item key={data.id} lg={4}>
                                <AnimalCard data={data} />
                            </Grid>
                        );
                    })}
                </Grid>
                <Grid container justify={"flex-end"} className={"link-view-more-container"}>
                    <Link to={"/lista-animais"} className={"link-view-more"}>Ver mais animais</Link>
                </Grid>
            </Grid>
        </Container>
    );
}

export default AdoptHomeBlock;