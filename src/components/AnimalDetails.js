import React, {Component, useEffect, useState} from 'react';
import {useParams} from "react-router";
import Header from "./Header";
import Footer from "./Footer";
import GalleryDetail from "./GalleryDetail";
import animalList from "../data/animalList";
import {Box, Breadcrumbs, Button, Card, Container, Grid, Paper, Typography} from "@material-ui/core";
import "../styles/AnimalDetails.scss";
import FacebookIcon from "@material-ui/icons/Facebook";
import WhatsAppIcon from "@material-ui/icons/WhatsApp";
import InstagramIcon from "@material-ui/icons/Instagram";
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import TwitterIcon from '@material-ui/icons/Twitter';
import {Link} from "react-router-dom";
import Map from "./Map";

const AnimalDetails = (props) => {

    let { id } = useParams();
    const animalDetails = animalList.find(d => d.id === parseInt(id));

    const [state, setState] = useState({
        mobileView: false
    });

    const { mobileView} = state;

    useEffect(() => {
        window.scrollTo(0, 0);
        const setResponsiveness = () => {
            return window.innerWidth < 900
                ? setState((prevState) => ({ ...prevState, mobileView: true }))
                : setState((prevState) => ({ ...prevState, mobileView: false }));
        };

        setResponsiveness();

        window.addEventListener("resize", () => setResponsiveness());
    }, []);


    const shareAndDonate = (
        <div>
            <div className={"paper-container"}>
                <h3>Partilhar</h3>
                <div className={"share-icons"}>
                    <FacebookIcon fontSize={"large"}/>
                    <WhatsAppIcon fontSize={"large"}/>
                    <InstagramIcon fontSize={"large"}/>
                    <TwitterIcon fontSize={"large"}/>
                    <MailOutlineIcon fontSize={"large"}/>
                </div>
            </div>
            <div className={"paper-container"}>
                <h3>Ajudar</h3>
                <p>
                    Ao doar está a apoiar os nossos voluntários a suportar os custos com alimentação e
                    cuidados de saúde dos animais até encontrarem um lar definitivo.
                </p>
                <Link to={"#"} className={"btn-donate"}>
                    Efetuar donativo
                </Link>
            </div>
        </div>
    );

    const desktop = () => {



    };

    return (
        <div>
            <Header />
            <GalleryDetail data={animalDetails.photos}/>
            <Container>
                <Breadcrumbs aria-label="breadcrumb" className={"breadcrumbs"}>
                    <Link className={"previous"} to={"/"}>
                        Página principal
                    </Link>
                    <Link className={"previous"} to={"/lista-animais"}>
                        Adotar
                    </Link>
                    <Typography className={"selected"} color="textPrimary">Detalhes</Typography>
                </Breadcrumbs>
            </Container>
            <Container className={"animal-details-content"}>
                <Grid container className={"details-header"} justify={"space-between"}>
                    <div>
                        <h1>{animalDetails.name}</h1>
                    </div>
                    <div className={"btn-adopt"}>
                        <Link to={"/adotar/"+ animalDetails.id}>
                            Adotar
                        </Link>
                    </div>
                </Grid>
                <Grid container className={"animal-description-container"} spacing={4}>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                        <div className={"paper-container"}>
                            <h3>Características</h3>
                            <p>
                                <strong>Animal: </strong> {(animalDetails.type === "cat") ? "Gato" : "Cão"} <br />
                                <strong>Idade: </strong> {animalDetails.age} <br />
                                <strong>Sexo: </strong> {animalDetails.gender} <br />
                                <strong>Tamanho: </strong> {animalDetails.size} <br />
                                <strong>Esterilizado: </strong> {animalDetails.sterilized} <br />
                                <strong>Chip: </strong> {animalDetails.sterilized} <br />
                            </p>
                        </div>
                        {mobileView ? '' : shareAndDonate}
                    </Grid>
                    <Grid item xs={12} sm={12} md={8} lg={8}>
                        <div className={"paper-container"}>
                            <h3>Descrição</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        </div>
                        <div className={"paper-container"}>
                            <h3>Localização</h3>
                            <Map />
                        </div>
                        {mobileView ? shareAndDonate : ''}
                    </Grid>
                </Grid>

            </Container>
            <Footer />
        </div>
    );

}

export default AnimalDetails;