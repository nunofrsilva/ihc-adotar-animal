import React, {Component} from "react";
import Header from "./Header";
import {Route} from 'react-router-dom'
import CarouselHome from "./CarouselHome";
import AdoptHomeBlock from "./AdoptHomeBlock";
import Footer from "./Footer";
import AnimalList from "./AnimalList";
import AnimalDetails from "./AnimalDetails";
import AdoptAnimal from "./AdoptAnimal";
import Page from "./Page";
import NotImplemented from "./NotImplemented";

class Main extends Component {
    render() {
        return (
            <div>
                <Route exact path={"/"} render={() => (
                    <div>
                        <Header/>
                        <CarouselHome />
                        <AdoptHomeBlock />
                        <Footer />
                    </div>
                )}/>
                <Route exact path={"/lista-animais"} render={() => <AnimalList />} />
                <Route exact path={"/animal/:id"} render={() => <AnimalDetails />} />
                <Route exact path={"/adotar/:id"} render={() => <AdoptAnimal />} />
                <Route exact path={"/politica-de-privacidade"} render={() => <Page />} />
                <Route exact path={"/not-implemented"} render={() => <NotImplemented />}/>
            </div>
        );
    }
}

export default Main;