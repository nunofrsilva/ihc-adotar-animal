import React from 'react';
import {Link} from "react-router-dom";
import {Grid, Paper, Typography} from "@material-ui/core";
import '../styles/AdoptHomeBlock.scss'

const AnimalCard = (props) => {
    return (
        <Link to={"/animal/" + props.data.id} className={"animal-container"}>
            <Paper className={"animal-paper-container"} elevation={3}>
                <Grid container>
                    <Grid item lg={4}>
                        <img src={props.data.photos[0]} className={"img-block"} />
                    </Grid>
                    <Grid item lg={8}>
                        <div className={"description"}>
                            <Typography gutterBottom variant="h5" component="h3">
                                {props.data.name}
                            </Typography>
                            <Typography variant="body1" component="p">
                                <strong>Raça: </strong> {props.data.breed}
                                <br />
                                <strong>Tamanho: </strong> {props.data.size}
                                <br />
                                <strong>Localização: </strong> {props.data.localization}
                            </Typography>
                        </div>
                    </Grid>
                </Grid>
            </Paper>
        </Link>
    );
};

export default AnimalCard;