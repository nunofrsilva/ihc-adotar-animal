import React from 'react';
import Carousel from 'react-material-ui-carousel'
import {Button, Card, CardContent, CardMedia, Grid, Paper, Typography} from '@material-ui/core'
import '../styles/CarouselHome.scss'

const CarouselHome = () => {

    const Banner = (props) => {
        const contentPosition = props.contentPosition ? props.contentPosition : "left"
        let totalItems = props.length ? props.length : (window.innerWidth < 600) ? 2 : 3;
        totalItems =  (window.innerWidth < 430) ? 1 : totalItems;

        const mediaLength = (window.innerWidth > 430) ? totalItems - 1 : 1;
        let items = [];
        const content = (
            <Grid item xs={12 / totalItems} key="content">
                <CardContent className="Content">
                    <Typography className="Title">
                        {props.item.Name}
                    </Typography>

                    <Typography className="Caption">
                        {props.item.Caption}
                    </Typography>

                    <Button variant="outlined" className="ViewButton">
                        Ver mais
                    </Button>
                </CardContent>
            </Grid>
        )


        for (let i = 0; i < mediaLength; i++) {
            const item = props.item.Items[i];

            const media = (
                <Grid item xs={12 / totalItems} key={item.Name}>
                    <CardMedia
                        className="Media"
                        image={item.Image}
                        title={item.Name}
                    >
                        <Typography className="MediaCaption">
                            {item.Name}
                        </Typography>
                    </CardMedia>

                </Grid>
            )

            items.push(media);
        }

        if (window.innerWidth > 430) {
            if (contentPosition === "left") {
                items.unshift(content);
            } else if (contentPosition === "right") {
                items.push(content);
            } else if (contentPosition === "middle") {
                items.splice(items.length / 2, 0, content);
            }
        }

        return (
            <Card raised className="Banner">
                <Grid container spacing={0} className="BannerGrid">
                    {items}
                </Grid>
            </Card>
        )
    }


    const items = [
        {
            Name: "Adotar um cão",
            Caption: "Adotar um cão é uma oportunidade magnífica de dar as boas-vindas a um novo membro da família, enquanto oferece a um animal uma segunda oportunidade de ter uma vida feliz.",
            contentPosition: "left",
            Items: [
                {
                    Name: "Alvan Nee",
                    Image: "https://images.unsplash.com/photo-1548199973-03cce0bbc87b?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80"
                },
                {
                    Name: "Cristian",
                    Image: "https://images.unsplash.com/photo-1576201836106-db1758fd1c97?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80"
                }
            ]
        },
        {
            Name: "Benefícios de ter um animal de companhia",
            Caption: "Juntos estamos de facto melhor! Partilhar a vida com um animal de companhia é divertido e cheio de momentos de afeto e brincadeira. Descubra os vários benefícios de partilhar a sua vida com um cão ou um gato.",
            contentPosition: "middle",
            Items: [
                {
                    Name: "Gatos Bebés",
                    Image: "https://images.unsplash.com/photo-1517331156700-3c241d2b4d83?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1348&q=80"
                },
                {
                    Name: "Kari Shea",
                    Image: "https://images.unsplash.com/photo-1491485880348-85d48a9e5312?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80"
                }
            ]
        },
        {
            Name: "Adote",
            Caption: "Existem milhares de animais a precisar de um lar em Portugal. Alguns foram abandonados, outros perderam-se dos donos e outros já nasceram nas ruas.",
            contentPosition: "right",
            Items: [
                {
                    Name: "Streefland",
                    Image: "https://images.unsplash.com/photo-1543466835-00a7907e9de1?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1267&q=80"
                },
                {
                    Name: "Sutton",
                    Image: "https://images.unsplash.com/photo-1529257414772-1960b7bea4eb?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80"
                }
            ]
        }
    ]

    const Item = (props) => {
        return (
            <Card className={"Banner"}>
                <Paper>
                    <h2>{props.item.name}</h2>
                    <p>{props.item.description}</p>

                    <Button className="CheckButton">
                        Check it out!
                    </Button>
                </Paper>
            </Card>
        )
    }

    return (
        <Carousel
            swipe={true}
            animation={"slide"}
            autoPlay={true}
            stopAutoPlayOnHover={true}
        >
            {
                items.map((item, index) => <Banner item={item} key={index} contentPosition={item.contentPosition} /> )
            }
        </Carousel>
    )
};

export default CarouselHome;